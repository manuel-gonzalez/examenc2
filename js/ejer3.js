const fileInput = document.getElementById('fileInput');
const imagenMostrada = document.getElementById('imagenMostrada');

fileInput.addEventListener('change', function (event) {
    const file = event.target.files[0];
    if (file) {
        const reader = new FileReader();

        reader.onload = function (e) {
            imagenMostrada.src = e.target.result;
            imagenMostrada.style.display = 'block';
        };

        reader.readAsDataURL(file);
    } else {
        imagenMostrada.src = '';
        imagenMostrada.style.display = 'none';
    }
});
