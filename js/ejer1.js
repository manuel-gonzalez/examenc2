function generarTabla() {
    var numero = parseInt(document.getElementById("numero").value);
    var tablaHTML = "</h2><table>";
    for (var i = 1; i <= 10; i++) {
        var resultado = numero * i;
        var operacion = numero + " x " + i + " = " + resultado;
        var operacionConImagenes = "";

        // Agregar imágenes para los dígitos y el signo de igual en la operación
        for (var j = 0; j < operacion.length; j++) {
            var caracter = operacion[j];
            if (caracter === " ") {
                operacionConImagenes += "&nbsp;";
            } else {
                operacionConImagenes += '<img src="/img/' + caracter + '.png" alt="' + caracter + '" width="50" height="50">';
            }
        }

        tablaHTML += "<tr><td>" + operacionConImagenes + "</td></tr>";
    }
    tablaHTML += "</table>";

    document.getElementById("tablaResultado").innerHTML = tablaHTML;
}
